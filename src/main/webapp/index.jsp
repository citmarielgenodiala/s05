<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>S05 Activity-b</title>
</head>
</head>
<style> 
	div{
		margin-top:5px;
		margin-bottom:5px
	}
</style>
<body>
	<h1>Welcome to Servlet Job Finder!</h1>
	
	<form action="register" method="POST">
		<div>
			<label for="firstname">First Name </label>
			<input type="text" name="firstname" required>
		</div>
		
		<div>
			<label for="lastname">Last Name</label>
			<input type="text" name="lastname" required>
		</div>
		
		<div>
			<label for="phone">Phone</label>
			<input type="tel" name="phone" required>
		</div>
		
		<div>
			<label for="email">Email</label>
			<input type="email" name="email" required>
		</div>
		
		<fieldset>
			<legend> How did you discover the app?</legend>
			<input type="radio" id="friends" name="app_discovery" required value="friends">
			<label for="friends">Friends</label>
			<br>
			
			<input type="radio" id="social_media" name="app_discovery" required value="socialmedia">
			<label for="social_media">Social Media</label>
			<br>
			<input type="radio" id="others" name="app_discovery" required value="others">
			<label for="others">Others</label>
		</fieldset>
		
		<div>	
			<label for="birth_date">Date of Birth </label>
			<input type="date" name="birth_date" required>	
		</div>
		
		<div>
			<label for="user_type">Are you an employer or applicant?</label>
			<input type="text" name="user_type" required list="userTypes">
			<datalist id="userTypes">
				<option value="Employer">
				<option value="Applicant">
			</datalist>
		</div>
		
		<div>
			<label for="profile_description">Profile Description </label>
			<textarea name="profile_description" maxlength="500" required></textarea>
		</div>
		<button>Register</button>
	</form>
	
</body>
</html>