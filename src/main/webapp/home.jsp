<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Home Page</title>
</head>
</head>
<style> 
	div{
		margin-top:5px;
		margin-bottom:5px
	}
</style>
  <body>
	 <% 
 	 	String fullName = session.getAttribute("fullName").toString();
	 	System.out.println("Welcome "+fullName+ "!");
	%>
	<h1>Welcome <%= fullName %>!</h1>
	<% 
		if (session.getAttribute("userType").equals("Employer")) { %>
	  		<p>Welcome employer. You may now start browsing applicant profile.</p>
		<% } 
		else { %>
	 		<p>Welcome applicant. You may now start looking for your career opportunity.</p>
		<% } 
	%>

  </body>
</html>