<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Registration Confirmation</title>
</head>
<body>
		<%
			String appDiscovery = session.getAttribute("appDiscovery").toString();
			if(appDiscovery.equals("friends")){
				appDiscovery="Friends";
			}
			else if(appDiscovery.equals("social media")){
				appDiscovery="Social Media";
			}
			else{
				appDiscovery="Others";
			} 
			
		%>
		<h1>Registration Confirmation</h1>
		<p>First Name: <%=session.getAttribute("firstname") %></p>
		<p>Last Name: <%=session.getAttribute("lastname") %></p>
		<p>Phone Number: <%=session.getAttribute("phone") %></p>
		<p>Email: <%=session.getAttribute("email") %></p>
		<p>App Discovery: <%=appDiscovery %></p>
		<p>Date of Birth: <%= session.getAttribute("birthDate")%></p>
		<p>User Type: <%= session.getAttribute("userType")%></p>
		<p>Description: <%= session.getAttribute("profileDescription")%></p>
		
		<form action="login" method="post">
			<input type="submit">
		</form>
		
		<form action="index.jsp" >
			<input type="submit" value="Back">
		</form>
</body>
</html>