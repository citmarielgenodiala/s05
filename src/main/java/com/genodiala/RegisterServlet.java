package com.genodiala;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.Period;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
	
	public void init() throws ServletException{
		System.out.println("RegisterServlet has been initialized.");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		PrintWriter out = res.getWriter(); 
		String firstname=req.getParameter("firstname");
		String lastname=req.getParameter("lastname");
		String phone=req.getParameter("phone");
		String email=req.getParameter("email");
		String appDiscovery=req.getParameter("app_discovery");
		String birthDate=req.getParameter("birth_date");
		String userType=req.getParameter("user_type");
		String profileDescription=req.getParameter("profile_description");
		
		//Additional condition to capitalized each word of the Firstname and Lastname
		String capitalizedFirstname = "";
		String[] words = firstname.toLowerCase().split("\\s+");
		for (String word : words) {
		    capitalizedFirstname += word.substring(0, 1).toUpperCase() + word.substring(1) + " ";
		}
		capitalizedFirstname = capitalizedFirstname.trim();

		String capitalizedLastname = "";
		words = lastname.toLowerCase().split("\\s+");
		for (String word : words) {
		    capitalizedLastname += word.substring(0, 1).toUpperCase() + word.substring(1) + " ";
		}
		capitalizedLastname = capitalizedLastname.trim();

		// Additional condition to check if the applicant is equal or greater than 18 yrs old, otherwise not eligible to access the servlet
		/*
		 * LocalDate birthdate = LocalDate.parse(birthDate); LocalDate now =
		 * LocalDate.now(); int age = Period.between(birthdate, now).getYears(); if (age
		 * < 18) { System.out.println("Not eligible");
		 * out.println("<div align='center'>" +
		 * "<h1> Error. You are not yet eligible to access this servlet. </h1>" +
		 * "</div>");
		 * 
		 * return; }
		 */
	    
		HttpSession session = req.getSession();
		session.setAttribute("firstname",capitalizedFirstname);
		session.setAttribute("lastname",capitalizedLastname);
		session.setAttribute("phone", phone);
		session.setAttribute("email", email);
		session.setAttribute("appDiscovery", appDiscovery);
		session.setAttribute("birthDate", birthDate);
		session.setAttribute("userType", userType);
		session.setAttribute("profileDescription", profileDescription);

		res.sendRedirect("register.jsp");
	}
	public void destroy() {
		System.out.println("RegisterServlet has been destroyed.");
	}
}
